[section .data]
num	dd	2147483647
formstr	db	"%s"
[section .bss]
buffer	resb	11

[section .text]
extern itoa
extern printf
global _main

_main:
push ebp           ; Set up stack frame for debugger 
mov ebp,esp 
push ebx           ; Program must preserve ebp, ebx, esi, & edi 
push esi
 push edi 

push dword 10
push dword buffer
mov eax, [num]
push eax
call itoa

add esp, 12

push dword buffer
push dword formstr
call printf

 ;;; Everything after this is boilerplate; use it for all ordinary apps! 
      pop edi            ; Restore saved registers 
      pop esi 
      pop ebx 
      mov esp,ebp        ; Destroy stack frame before returning 
      pop ebp 
      ret                ; Return control to Linux 

