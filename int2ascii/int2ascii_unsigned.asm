[section .data]
num	dd	-92968	;number to convert

[section .bss]
ascii	resb	11	;buffer for ascii

[section .text]
global _main
_main:

	mov	cx, 10		;set max iterations (10 digits.. for MAX_INT)
	mov	eax, [num]	;put number in EAX to do the conversion
.loop1:
	mov	edi, ecx	;move to edi so we can use as offset
	dec	edi		;0 based addressing needs dec

	xor	edx, edx	;since idiv works on edx:eax, edx must be 0'd
	mov	ebx, 10		;idiv only works on reg/mem, no imm
	idiv	ebx		;get rightmost digit in EDX, rest in EAX
	or	edx, 0x30	;make ascii rather than just BCD
	mov	[ascii+edi], dl ;save that digit to the buffer
	and	eax,eax		;set flags based on contents of eax
	jz	.done		;no more number, we are done
	loop	.loop1		;else loop
.done:
	dec	ecx		;LOOP never reached, so CX needs down 1 more
	mov	edi, 10		;we are determining num length, 10 is max
	sub	edi, ecx	;subtract num iterations not done.. find length

	mov	[ascii+10], byte 0xa ;throw a newline on the end
	inc	edi

	push	edi
	mov	esi, ascii
	add	esi, ecx
	push	esi
	push	1
	mov	eax, 4
	push	eax
	int	0x80

	add	esp, 16

	push	0
	mov	eax, 1
	push	eax
	int 0x80
		
