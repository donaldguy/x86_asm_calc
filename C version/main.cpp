#include <stack>
#include <cstdio>
#include <cctype>
#include <cstdlib>

using namespace std;

// I am simulating CPU registers and stack
stack<int> theStack = stack<int>(); //here is our stack
long eax, ebx, ecx, edx; //long is used because it is 32bit like our regs

void getInput();

int main (int argc, char * const argv[]) {
    
	//first thing to do is get input (hopefully we end up with nums on the stack)
	getInput();
	

	ebx = theStack.top(); theStack.pop(); // this is arg2
	eax = theStack.top(); theStack.pop(); // this is the operator
	switch (eax)
	{
		case '+':
				eax = theStack.top(); theStack.pop(); // now we have arg1
				eax += ebx;
				break;
		case '-':
				eax = theStack.top(); theStack.pop(); // now we have arg1
				eax -= ebx;
				break;
		case '*':
				eax = theStack.top(); theStack.pop(); // now we have arg1
				eax *= ebx;
				break;
		case '/':
				eax = theStack.top(); theStack.pop(); // now we have arg1
				eax /= ebx;
				break;
	}
		

	printf("%d\n",eax); //if only the IA32 had a PRINTF 
	
	return 0;
}

void getInput() 
{
	char c; //if I was truly playing the accuracy game I would use al
			// but then I'd need to keep up the al is part of eax rules
	ebx = 0;
	
	while(1)
	{
		c = getchar();
		
		if ( c == ' ') 
			continue; 
		
		if (isdigit(c)) 
		{
			ebx *= 10;
			ebx += c ^ 0x30;
		}
		
		if ( c == '+' || c == '-' || c == '*' || c == '/')
		{
			theStack.push(ebx);
			theStack.push(c);
			ebx = 0;
		}
		
		if ( c == '\n' )
		{
			theStack.push(ebx);
			break;
		}
	}
	
}