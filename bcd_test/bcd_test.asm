[section .data]
add1	db	"2"
add2	db	"2"

[section .bss]
sum	resb	1

[section .text]

global _start
_start:
	mov	al, [add1]
	add	al, [add2]
	aaa			;ascii adjust
	or	al, 0x30
	mov	[sum], byte  ax	;add things and save to memory

	;print the sum
	push	1		; 1 byte
	push	sum		; adress of sum
	push	1		; stdout
	mov	eax, 4		; SYS_write
	push	eax		; junk
	int	0x80		; invoke kernel
	
	add	esp, byte 16	;

	push	0		; return status
	mov	eax, 1		; SYS_exit
	push	eax		; junk
	int 	0x80


